# Summary

* [Introduction](./README.md)
* [General Notes](./NOTES.md)
* [Competition & Inspiration](./INSPIRATION.md)

## References
* [Primary Datasources](./references/PRIMARY_DATASOURCES.md)
* [Additional Potential Datasources](./references/OTHER_DATASOURCES.md)
* [Relevant Organizations](./references/ORGANIZATIONS.md)
* [Government & Oversight](./references/GOVERNMENT.md)
* [Plants & Ecology](./references/PLANTS.md)
* [Suppliers](./references/SUPPLIERS.md)
* [Traditional Chinese Medicine](./references/TCM.md)
* [Agriculture](./references/AGRICULTURE.md)
* [Read/Review](./references/READ_REVIEW.md)

