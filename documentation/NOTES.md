# Notes

A place for not yet organized notes and ideas.

---

## From initial discussion:

- Reference TakeCareOf.com as a template for V1.
- Link to pub med search results for each species as an initial offering of more scientific resources.
- Include ethnobotanical history? When was the medicinal use brought into western consciousness?
- Integrate 23 & Me genetic information.
  - API available?
- Areas of Wellness to Consider:
  - Fertility, an important subject among middle class professional women.
- Consider focusing on men's health?
  - Testosterone and other hormone increase or balance.
  - Bronze Age Herbalist and Raw Egg Nationalist are both popular anonymous twitter accounts that may reveal a market entrypoint.
- Use Google Search Trends to help in search indexing and design.

- Maintain user accounts in initial version?
  - For preferences, personal data, etc.

- Global map that allows to interactively explore regions and remedies.
  - Use GBIF API to geolocate each region.

- Amazon Affiliate Program
  - Bundle subscriptions?
  - Are recurring subscriptions traced back to us?
  - Manage subscriptions on our site through API?

- Potential future marketplace for herbs?
  - ETSY for herbalism?

## General Thoughts

One thing we could lean on is the democratic nature of traditional medicine. People can grow their own medicine, or buy it generically from their neighbors. No research or manufacturing laboratories, no FDA or protectionary regulation. This falls in line with information about foraging and maybe growing and processing guides.

## Potential Names
- Herb.com
  - At one point this was a known internet forum but now the domain is for sale.
- DeadNettle.com
  - Lamium purpureum (Dead Nettle, Purple Archange) is a common meadow weed. Kind of an edgy name.