# Agriculture

### [USDA National Agricultural Statistics Service](https://www.nass.usda.gov/Quick_Stats/)
> The Quick Stats Database is the most comprehensive tool for accessing agricultural data published by NASS. It allows you to customize your query by commodity, location, or time period. You can then visualize the data on a map, manipulate and export the results, or save a link for future use.
Covers nationwide agricultural data as recently as 2017, and going back through each census into the 1800s.
#### There may be an opportunity here to build a product for analyzing agricultural futures.
https://data.world/us-usda-gov/3c567bb3-08f5-4bb8-87fd-5b645b8a769c