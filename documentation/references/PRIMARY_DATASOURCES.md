# Primary Data Sources

Below are the two primary and authoritative databases of herbs and natural medicines. Though they require an annual fee (>$200), they should provide enough information for an initial product.

---

### [Natural Medicines Comprehensive Database](https://naturalmedicines.therapeuticresearch.com/)
#### Cost: $182/year
*Related Research Paper: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC64772/*
> One of the most comprehensive and reliable natural medicine resources available by Therapeutic Research Faculty
> - Interactive tools to find information on safety, effectiveness, and interactions
> - Over 1,400 natural medicine monographs
> - Intuitive, industry-leading NMBER® rating system for 185,000+ commercial brand products
> - CE for condition/disease state clinical management using natural or alternative medicines

> The Natural Medicines Comprehensive Database, one of the most comprehensive and reliable natural medicine resources available, is by Therapeutic Research Faculty, an impressive team of experts. Natural medicines in this context refer to all herbal and nonherbal supplements. Although some of the supplements may not be produced from natural sources, they are included, because they are generally categorized with natural products.[^1]

#### Databases

##### [Food, Herbs & Supplements](https://naturalmedicines.therapeuticresearch.com/databases/food,-herbs-supplements)
###### Access: Webscraping
```graphql
# https://naturalmedicines.therapeuticresearch.com/databases/food,-herbs-supplements/professional.aspx?productid=953
type Herb{
# Ashwagandha
name: String
# 953
productId: String
# https://naturalmedicines.therapeuticresearch.com/databases/food,-herbs-supplements/professional.aspx?productid=953
url: String
# Withania somnifera
scientificName: String
# [Physalis somnifera]
synonyms: [String]
# Solanaceae
family: String
# [Ajagandha, Amangura, Amukkirag, Asan, Asana]
commonNames: [String]
# Ashwagandha should not be confused with another plant commonly known as [Winter Cherry](link). Also, avoid confusion with other herbs used as adaptogens, such as [American Ginseng](link), [Panax Ginseng](link), and [Eleuthero](link).
caution: String
overview: Overview
safety: Safety
adverseAffects: AdverseAffects
effectiveness: [Effect]
dosing: DosingAndAdministration
pharmaceuticalInteractions: [PharmaceuticalInteraction]
supplementInteractions: [SupplementInteraction]
conditionalInteractions: [ConditionalInteraction]
labTestInteractions: [LabTestInteraction]
# There is insufficient reliable information available about the presentation or treatment of overdose with ashwagandha.
overdose: String
commercialProducts: [CommercialProduct]
canadianLicensedCommercialProducts: [CommercialProduct]
# There is insufficient reliable information available about the pharmacokinetics of ashwagandha.
pharmacokinetics: String
mechanismsOfAction: [Mechanism]
classifications: [Classification]
monographReferences: [MonographReference]
}

type Overview {
# Ashwagandha is a small evergreen shrub found in dry areas of India and the Middle East, as well as parts of Africa (14863,32533). Ashwagandha is used in traditional medicine, usually as an adaptogenic herb (14863,32525,90650,90651). Adaptogens are theorized to help the body resist physiological and psychological stress.
summary: String
# Ashwagandha is sometimes substituted or adulterated with a similar plant, Withania coagulans (3710).
warnings: String
}

type Safety {
# POSSIBLY SAFE ...when used orally and appropriately, short-term. Ashwagandha has been used with apparent safety in doses of up to 1250 mg daily for up to 6 months (3710,11301,19271,90649,90652,90653,97292,101816,102682,102683) (102684,102685,102687,103476,105824).
summary: String
# LIKELY UNSAFE ...when used orally. Ashwagandha has abortifacient effects (12).
pregnancy: String
# Insufficient reliable information available; avoid using.
lactation: String
}

type AdverseAffects {
# Orally, ashwagandha seems to be well-tolerated. Topically, no adverse effects have been reported. However, a thorough evaluation of safety outcomes has not been conducted.
general: String
# Orally: Diarrhea, gastrointestinal upset, nausea, and vomiting. However, these adverse effects do not commonly occur with typical doses.
mostCommon: [String]
# Orally: Some case reports raise concerns about liver injury with ashwagandha treatment.
serious: [String]
}

enum EffectivenessRating {
LIKELY_EFFECTIVE
POSSIBLY_EFFECTIVE
POSSIBLY_INEFFECTIVE
INSUFFICIENT_RELIABLE_EVIDENCE_TO_RATE
}

type Effect {
# Insomnia
condition: Condition
# Oral ashwagandha may modestly improve sleep in patients with insomnia or non-restorative sleep.
description: String
# A small meta-analysis in patients with insomnia and healthy patients shows that taking a specific ashwagandha root extract (KSM-66, Ixoreal Biomed) 250-600 mg daily or a specific root and leaf extract (Shoden, Arjuna Natural) 120 mg daily for 6-12 weeks modestly improves overall sleep, as well as sleep quality, sleep latency, total sleep time, wake time after sleep onset, and sleep efficiency, when compared with placebo. Effects were more pronounced in those with insomnia, with doses of at least 600 mg daily, and with treatment durations of at least 8 weeks (106784). These findings may be limited by heterogeneity of the included studies. In patients with non-restorative sleep, clinical research shows that taking a specific ashwagandha extract (Shoden, Arjuna Natural Private Ltd) 125 mg daily for 6 weeks increases sleep quality by 72%, compared with an improvement of 29% in patients taking placebo. Small to moderate improvements also occurred in total sleep time, sleep latency, and number of times awake (103476).
details: String
}

type DosingAndAdministration {
# Oral:
# Ashwagandha has most often been used in doses of up to 1000 mg daily for up to 12 weeks. See Effectiveness section for condition-specific information.
# Ashwagandha extract is often standardized to withanolide content, with concentrations ranging from 1.5% to 35% (19271,90650,90652,90653,106786).
adult: String
# Oral:
# Research is limited; typical dosing is unavailable.
children: String
# Ashwagandha root by Swiss Herbals, used in some clinical trials, has been standardized to 1.5% withanolides (19271). Another standardized product is Sensoril (Natreon, Inc.), containing a minimum of 8% withanolides and 32% oligosaccharides, and a maximum of 2% withferin A (90653). KSM-66 (Ixoreal Biomed) is standardized to withanolide ≥ 5% and contains alkaloids, the amino acids threonine, valine, methionine, isoleucine, lysine, aspartic acid, and arginine, complex sugars such as oligosaccharides and fructooligosaccharides, and vitamin A, calcium, and iron (90650,90652). Shoden (Arjuna Natural Inc) is standardized to 35% withanolide glycosides (101816,103476,106786).
standardizationAndFormulation: string
}

type PharmaceuticalInteraction {
# ANTIDIABETES DRUGS
name: String
description: String
# Moderate. Be cautious with this combination.
rating: String
# High
severity: String
# Possible
Occurrence: String
# B
levelOfEvidence: String
# Theoretically, taking ashwagandha with antidiabetes drugs might increase the risk of hypoglycemia.
details: String
}

type SupplementInteraction {
# HERBS AND SUPPLEMENTS WITH HYPOTENSIVE EFFECTS
name: String
# Theoretically, ashwagandha might have hypotensive effects.
description: String
# Animal research suggests that ashwagandha might lower blood pressure (19279). Theoretically, combining ashwagandha with other herbs and supplements with hypotensive effects might increase the risk of hypotension.
details: String
}

type ConditionalInteraction {
# AUTOIMMUNE DISEASES
name: String
# Ashwagandha has demonstrated immunostimulant effects in humans (3710,3711,4114,11301,106786). Theoretically, ashwagandha might exacerbate autoimmune diseases by stimulating immune activity. Until more is known, avoid or use with caution in patients with autoimmune diseases such as multiple sclerosis, systemic lupus erythematosus (SLE), rheumatoid arthritis (RA), or others.
details: String
}

type LabTestInteraction {
# DIGOXIN SERUM ASSAY
name: String
# Taking ashwagandha might result in falsely elevated digoxin levels in some serum assays. Ashwagandha contains withaferin A, which has a similar structure to digoxin (14863,16417). Ashwagandha can falsely elevate digoxin levels when using fluorescence polarization immunoassays (FPIA), microparticle enzyme immunoassays (MEIA) or the Abbott Digoxin III assay (14863,16417). The Beckman assay for digoxin seems to be only minimally affected (14863). The Roche Tina-Quant turbidimetric inhibition immunoassay is only affected by very high ashwagandha levels, equivalent to plasma levels after an overdose (16417).
details: String
}

type Mechanisms {
# Anticancer effects
effect: String
# Ashwagandha is of interest for the treatment of cancer. In laboratory research, the withanolides and ashwagandha extract had cytotoxic effects in cancer cells and other laboratory models (32509,32517,32518,32538,32548,32551,32552).
description: String
}

type CommercialProduct{
name: String
# 1-10, with 10 the highest rated
rating: Int
primaryIngredient: [Herb]
dosage: String
otherIngredients: [String]
}

type Classification {
# Hypoglycemic Agents
name: String
# https://naturalmedicines.therapeuticresearch.com/tools/charts/chartdisplay.aspx?classificationID=26
url: String
herbs: [Herb]
}

type MonographReference {
# 12
number: Int
# McGuffin M, Hobbs C, Upton R, Goldberg A, eds. American Herbal Products Association's Botanical Safety Handbook. Boca Raton, FL: CRC Press, LLC 1997.
citation: String
}

```

##### [Comparative Effectiveness](https://naturalmedicines.therapeuticresearch.com/databases/comparative-effectiveness)
```graphql
# https://naturalmedicines.therapeuticresearch.com/databases/comparative-effectiveness/condition.aspx?condition=Abdominal+pain
type Therapy {
    # Abdominal Pain
    condition: String
    # Lacticaseibacillus rhamnosus
    name: String
    # INSUFFICIENT_RELIABLE_EVIDENCE_TO_RATE
    effectivenessRating: EffectivenessRating
}

enum EffectivenessRating {
    POSSIBLY_EFFECTIVE
    POSSIBLY_INEFFECTIVE
    INSUFFICIENT_RELIABLE_EVIDENCE_TO_RATE
}
```


---


### [American Botanical Council](https://www.herbalgram.org/)

##### Cost: $150/year
> COMPLETE access to ABC's online resources, including
> - HerbalGram Online, including searchable archives since 1990
> - The Complete German Commission E Monographs Online
> - Healthy Ingredients
> - HerbClip Online, with more than 8,000 reviews, with 32 new reviews added each month
> - HerbMedPro, including clinical studies and data on more than 270 herbs
> - The ABC Clinical Guide to Herbs online searchable database.
> - The Identification of Medicinal Plants: A Handbook of the Morphology of Botanicals in Commerce online

#### [THE COMPLETE GERMAN COMMISSION E MONOGRAPHS](https://www.herbalgram.org/resources/expanded-commission-e/)

##### API: Webscraping

```graphql
type Species {
# Ex: Angelica root
name: String
# Ex: Angelica archangelica
latinName: String
# Ex: Angelicae radix
pharmacopeialName: String
# Ex: European angelica
otherNames: [String!]
overview: String
# Ex: Angelica root consists of the dried root and rhizome of A. archangelica L. [Fam. Apiaceae], and their preparations in effective dosage. The root and rhizome contain essential oil, coumarin, and coumarin derivatives.
description: String
chemistryAndPharmacology: String
# Ex: The German Standard License indicates the use of angelica root tea for treatment of complaints such as feeling of fullness, flatulence, and mild cramp-like gastrointestinal disturbances, as well as stomach conditions such as insufficient formation of gastric juice (Braun et al., 1997). In India, it is used to treat anorexia nervosa and flatulent dyspepsia (Karnick, 1994).
uses: String
contraindications: String
interactions: String
# Ex: The furanocoumarins present in angelica root sensitize the skin to light. Subsequent exposure to UV radiation can lead to inflammation of the skin. During treatment with the drug or its preparations, prolonged sun-bathing and exposure to intense UV radiation should be avoided.
sideEffects: String
useDuringPregnancyAndLactation: String
dosageAndAdministration: Dosage
references: String
additionalResources: String
}

type Dosage {
decoction: String
infusion: String
fluidExtract: String
tincture: String
essentialOil: String
}
```

#### [ABC Clinical Guide to Herbs](https://www.herbalgram.org/resources/abc-clinical-guide)
> The ABC Clinical Guide to Herbs is based on the science-based, peer-reviewed text of the same name. It contains complete therapeutic monographs and clinical study details on 30 of the best-selling herbs in the United States at the time it was published, 2003. Most of these herbs are still found in the lists of top-selling herbs used as teas or dietary supplements.
> The online version allows members to download PDFs of the full monographs — including the main monograph, the clinical overview, and the patient information sheet for each herb — as well as PDFs of the proprietary product monographs.

#### [Herb Med Pro](https://www.herbalgram.org/resources/herbmedpro/)
> An interactive, electronic herbal database provides hyperlinked access to the scientific data underlying the use of herbs for health. An evidence-based information resource for professionals, researchers, and general public.

> It is an enhanced version of HerbMed®, provides access to the entire database (225 herbs) with continuous updating using Dynamic Updates, and is available for subscription and licensing or multiuser subscriptions from the American Botanical Council.[^1]

---