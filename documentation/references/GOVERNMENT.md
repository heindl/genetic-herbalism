# Government & Oversight

### [Licensed Natural Health Products Database](https://health-products.canada.ca/lnhpd-bdpsnh/index-eng.jsp)
Contains information about natural health products (dietary supplements, herbal medicines, traditional medicines, homeopathic remedies, etc.) that have been issued a product license by Health Canada.

---

### [Natural Health Products Ingredients Database](http://webprod.hc-sc.gc.ca/nhpid-bdipsn/monosReq.do?lang=eng)
This is a portion of the Natural Health Products Ingredients Database, maintained by Health Canada that covers single ingredients commonly used in herbal preparations; single ingredients includes chemical compounds and plants.
It provides a list of monographs for a number of herbal supplements.

---

### [NIH Dietary Supplement Label Database](https://ods.od.nih.gov/Research/Dietary_Supplement_Label_Database.aspx)
The Dietary Supplement Label Database (DSLD) was developed by the Office of Dietary Supplements (ODS) at the National Institutes of Health (NIH). It catalogs all information printed on labels of dietary supplement products sold in the United States. The database resulted from specific recommendations to NIH from Congress in 2004 that encouraged the Office to develop, create, regularly update, maintain, and make available to government and research entities a database of all supplement labels sold in the United States. The DSLD is available at https://dsld.od.nih.gov.
Dietary Supplement product information in the DSLD includes:
- Images of package labels
- Name and form of ingredients
- Amount of dietary ingredients
- All label statements

---

### [USP Herbal Medicines Compendium](https://hmc.usp.org/)
> Public standards are critically important to help ensure the quality of all medicines, including herbal medicines. The Herbal Medicines Compendium (HMC), published by the U.S. Pharmacopeial Convention (USP), is a freely available, online resource that provides standards for herbal ingredients used in herbal medicines. Standards are expressed primarily in monographs. A monograph contains general information including the definition of the herbal ingredient relative to the monograph title and then follows with a specification. The specification contains tests for critical quality attributes of the herbal ingredient and includes analytical test procedures and acceptance criteria for specified tests. USP’s HMC employs validated analytical procedures for the tests specified in its monographs, using state-of-the-art analytical techniques and allied reference materials. Additional analytical methods and approaches may be referenced in general chapters, which also are available online.

---

### [USDA Food and Nutrition Information Center](https://www.nal.usda.gov/programs/fnic)
Potentially good information about labelling, safety, security, food systems and business regulations.

---


### [NIH National Center for Complementary and Integrative Health: Herbs at a Glance](https://www.nccih.nih.gov/health/herbsataglance)
> Herbs at a Glance is a series of brief fact sheets that provides basic information about specific herbs or botanicals—common names, what the science says, potential side effects and cautions, and resources for more information.
- Contains a limited number of common herbs.

#### API: Web Scrape

```graphql
# https://www.nccih.nih.gov/health/acai
type Herb {
# Acai
name: String
# acai, acai berry, açaí
commonNames: [String]
# Euterpe oleracea, Euterpe badiocarpa
latinNames: [String]
# The acai palm tree, native to tropical Central and South America, produces a deep purple fruit. The acai fruit has long been an important food source for indigenous peoples of the Amazon region.
background: [String]
# Very little research has been done in people on the health effects of acai products.
howMuchDoWeKnow: [String]
# There's no definitive scientific evidence based on studies in people to support the use of acai for any health-related purpose.
whatHaveWeLearned: [String]
# Acai pulp appears to be safe when consumed in the amounts commonly used in foods; however, drinking unprocessed acai juice has been linked to an illness called American trypanosomiasis (also known as Chagas disease).
whatDoWeKnowAboutSafety: [String]
}
```

---

### [FDA Dietary Supplements](https://www.fda.gov/food/dietary-supplements)
- Information on selected dietary supplement products, ingredients, and other substances.
- Includes consumer updates, information from other agencies, and other educational materials for dietary supplement users.

---

### [NIH Drug Interaction API](https://lhncbc.nlm.nih.gov/RxNav/APIs/InteractionAPIs.html)
> The Interaction API is a web service for accessing drug-drug interactions.
> Currently, the API uses two sources for its interaction information - ONCHigh and DrugBank.
> ONCHigh is a list of high-priority drug-drug interactions derived by a panel of experts and contained in a JAMIA article. ONCHigh is updated using CredibleMeds with drugs that have a known risk of Torsades de Pointes (TdP).
> DrugBank drug-drug interaction information is drawn (unmodified) from the non-commercial version of the DrugBank database, which does not carry severity information. See the DrugBank license for more information.
> The drug-drug interactions are updated monthly, usually on the first Monday of the month.

##### API: REST

```xml
interactiondata: Root element in XML, or anonymous object in JSON
nlmDisclaimer:
interactionTypeGroup (array): Interactions from one data source
sourceDisclaimer:
sourceName: Source of interaction data (ONCHigh or DrugBank)
interactionType (array):
comment:
minConceptItem: RxNorm concept
rxcui: RxNorm identifier
name: RxNorm concept name
tty: RxNorm concept's term type
interactionPair (array): Two interactionConcepts
interactionConcept (array): RxNorm concept and corresponding drug from an interaction-source data set
minConceptItem: RxNorm concept
rxcui: RxNorm identifier
name: RxNorm concept name
tty: RxNorm concept's term type
sourceConceptItem: Drug from the interaction source's data
id: Source's identifier for the drug
name: Source's name for the drug
url: Location of information from the source of interaction data
severity: Severity (if provided by the source)
description:
```
