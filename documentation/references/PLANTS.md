# General Resources about Plants & Ecology

### [The Biota of North America Program](http://bonap.net/napa)
BONAP's North American Plant Atlas provides geospatial tools for understanding the history of the north american ecosystem, divided by species.

---

### [Rain-Tree Publishers Tropical Plant Database](https://www.rain-tree.com/plants.htm)
> Since 1996, Raintree has been dedicated to providing accurate and factual information on the important plants of the Amazon Rainforest, therefore this section of the Raintree web site is the most extensive.

---

### [USDA Plants Database](https://plants.sc.egov.usda.gov/home)
The PLANTS Database provides standardized information about the vascular plants, mosses, liverworts, hornworts, and lichens of the U.S. and its territories.

---

### [JSTOR Global Plants](https://plants.jstor.org/)
Global Plants is the world’s largest database of digitized plant specimens and a locus for international scientific research and collaboration.

---


### [North Carolina Extension Gardener Plant Toolbox](https://plants.ces.ncsu.edu/find_a_plant/)
The North Carolina Extension Gardener Plant Toolbox contains detailed descriptions and photographs of 4,631 plants that grow in and around North Carolina.
##### API: Web Scrape
```go
type Plant struct {
CommonNames string `json:"common_names"`
CountryOrRegionOfOrigin                             string `json:"country_or_region_of_origin,omitempty"`
DeciduousLeafFallColor                              string `json:"deciduous_leaf_fall_color,omitempty"`
DisplayHarvestTime                                  string `json:"display/harvest_time,omitempty"`
Distribution                                        string `json:"distribution,omitempty"`
Family          string          `json:"family,omitempty"`
FlowerBloomTime FlowerBloomTime `json:"flower_bloom_time,omitempty"`
FlowerDescription                                   string `json:"flower_description,omitempty"`
FlowerInflorescence string          `json:"flower_inflorescence,omitempty"`
FlowerSize          AppendageLength `json:"flower_size,omitempty"`
FruitDescription                                    string `json:"fruit_description,omitempty"`
FruitType  string `json:"fruit_type,omitempty"`
Genus      string `json:"genus"`
GrowthRate string `json:"growth_rate,omitempty"`
HabitForm  string `json:"habit/form,omitempty"`
HairsPresent                                        string `json:"hairs_present"`
LandscapeLocation string `json:"landscape_location,omitempty"`
LeafArrangement string `json:"leaf_arrangement,omitempty"`
LeafColor       string `json:"leaf_color,omitempty"`
LeafDescription                string `json:"leaf_description,omitempty"`
LeafLength AppendageLength `json:"leaf_length,omitempty"`
LeafMargin string          `json:"leaf_margin,omitempty"`
LeafShape  string          `json:"leaf_shape,omitempty"`
LeafType   string          `json:"leaf_type,omitempty"`
LeafWidth  AppendageLength `json:"leaf_width,omitempty"`
LifeCycle  string          `json:"life_cycle,omitempty"`
Light       string          `json:"light,omitempty"`
Maintenance string `json:"maintenance,omitempty"`
Name        string `json:"name"`
NcRegion                       string `json:"nc_region"`
PlantType string `json:"plant_type"`
Problems                       string `json:"problems,omitempty"`
RecommendedPropagationStrategy string `json:"recommended_propagation_strategy,omitempty"`
SoilDrainage                   string `json:"soil_drainage,omitempty"`
SoilTexture                    string `json:"soil_texture,omitempty"`
Species string `json:"species,omitempty"`
StemColor                      string `json:"stem_color,omitempty"`
StemDescription                string `json:"stem_description,omitempty"`
StemIsAromatic                 string `json:"stem_is_aromatic,omitempty"`
StemSurface                    string `json:"stem_surface,omitempty"`
WildlifeValue                  string `json:"wildlife_value,omitempty"`
Attracts                       string `json:"attracts,omitempty"`
BarkColor                      string `json:"bark_color,omitempty"`
CausesContactDermatitis        string `json:"causes_contact_dermatitis,omitempty"`
FlowerColor string `json:"flower_color,omitempty"`
FlowerPetals                   string `json:"flower_petals,omitempty"`
LeafFeel                       string `json:"leaf_feel,omitempty"`
PoisonPart                     string `json:"poison_part,omitempty"`
PoisonSeverity                 string `json:"poison_severity,omitempty"`
PoisonSymptoms                 string `json:"poison_symptoms,omitempty"`
PoisonToxicPrinciple           string `json:"poison_toxic_principle,omitempty"`
ResistanceToChallenges         string `json:"resistance_to_challenges,omitempty"`
StemForm                       string `json:"stem_form,omitempty"`
Texture                        string `json:"texture,omitempty"`
UsdaPlantHardinessZone         string `json:"usda_plant_hardiness_zone,omitempty"`
AvailableSpaceToPlant string `json:"available_space_to_plant,omitempty"`
BarkDescription                string `json:"bark_description,omitempty"`
DesignFeature string  `json:"design_feature,omitempty"`
Dimensions    string  `json:"dimensions,omitempty" csv:"-"`
Height        float64 `json:"-"`
Width         float64 `json:"-"`
FlowerShape                    string `json:"flower_shape,omitempty"`
FlowerValueToGardener string `json:"flower_value_to_gardener,omitempty"`
FruitColor                     string `json:"fruit_color,omitempty"`
FruitLength                    string `json:"fruit_length,omitempty"`
FruitWidth                     string `json:"fruit_width,omitempty"`
LandscapeTheme                                      string `json:"landscape_theme,omitempty"`
ParticularlyResistantToInsectsDiseasesOtherProblems string `json:"particularly_resistant_to_(insects/diseases/other_problems),omitempty"`
PlayValue                                           string `json:"play_value,omitempty"`
SoilPh                                              string `json:"soil_ph,omitempty"`
StemBudTerminal                                     string `json:"stem_bud_terminal,omitempty"`
UsesEthnobotany                                     string `json:"uses_(ethnobotany),omitempty"`
WoodyPlantLeafCharacteristics                       string `json:"woody_plant_leaf_characteristics,omitempty"`
Edibility                                           string `json:"edibility,omitempty"`
FireRiskRating                                      string `json:"fire_risk_rating,omitempty"`
FruitValueToGardener string `json:"fruit_value_to_gardener,omitempty"`
LeafValueToGardener  string `json:"leaf_value_to_gardener,omitempty"`
SurfaceAttachment                                   string `json:"surface/attachment,omitempty"`
StemBuds                                            string `json:"stem_buds,omitempty"`
StemBudScales                                       string `json:"stem_bud_scales,omitempty"`
StemLenticels                                       string `json:"stem_lenticels,omitempty"`
BarkPlateShape                                      string `json:"bark_plate_shape,omitempty"`
StemLeafScarShape                                   string `json:"stem_leaf_scar_shape,omitempty"`
StemCrossSection                                    string `json:"stem_cross_section,omitempty"`
ClimbingMethod string `json:"climbing_method,omitempty"`
Appendage                                           string `json:"appendage,omitempty"`
PithSplitLongitudinally                             string `json:"pith_(split_longitudinally),omitempty"`
BulbStorage                                         string `json:"bulb_storage,omitempty"`
}
```

---

### [NatureServe Explorer](https://explorer.natureserve.org/api-docs/)
NatureServe Explorer is the largest online encyclopedia of biodiversity in North America.
