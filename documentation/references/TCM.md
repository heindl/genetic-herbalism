# References Specific to Traditional Chinese Medicine

### [China Traditional Chinese Medicine Patent Database](https://www.sciencedirect.com/science/article/abs/pii/S0172219003001108)
- https://www.sciencedirect.com/science/article/abs/pii/S0172219003001108
> The deep indexed China Traditional Chinese Medicine (TCM) Patent Database was established by the State Intellectual Property Office (SIPO) of People’s Republic of China. The purpose of creating this database was mainly to meet the need of patent examination. The database has already been put to use in the patent examination department in SIPO since April 2002. The Chinese version of the database covers TCM-related patent applications published from 1985 till today in China. It contains over 19,000 bibliographic records and over 40,000 TCM formulas. [^1]

---

### [Traditional Chinese Medicine Basics](http://tcmbasics.com/materiamedica.htm)
Materia medica with 116 herbs.

---

### [SymMap](http://www.symmap.org/)
> SymMap integrates traditional Chinese medicine (TCM) with modern medicine (MM) through both internal molecular mechanism and external symptom mapping, thus provides massive information on herbs/ingredients, targets, as well as the clinical symptoms and diseases they are used to treat for drug screening efforts.
- 2,518 TCM symptoms
- 698 herbs
- 25,975 ingredients.
#### API: Bulk data download, free

---

### [Chinese herbal medicine database](https://www.meandqi.com/herb-database)
- Tools and resources to understand Traditional Chinese Medicine

#### API: Webscrape

```graphql
type Herb {
# Ai Ye
name: String
# Silvery wormwood leaves
english: String
# 艾叶
chinese: String
# Artemisia argyi
scientificName: String
# Chinese mugwort, Argy wormwood, Ai Cao or Gaiyou
otherNames: [String!]
# Dried leaves
partsUsed: String
# Herbs that invigorate the Blood
tcmCategory: String
# Warm
tcmNature: String
# Bitter, Pungent
tcmTastes: [String!]
# Spleen, Kidney, Liver
organAffinities: [String!]
# Remove impurities and dry.
preparation: String
# 3-9 grams
dosage: String
# Warms the meridians and stops bleeding. Warms the womb and calms the fetus. Expels Cold and stops pain. Calms cough, relieves asthma and breaks up Phlegm.
mainActions: [String!]
# Irregular menstruation, Abdominal pain, Hematemesis, Infertility, Abnormal uterine bleeding, Itching Nosebleed
primaryConditions: [String!]
# This herb should not be used when there is Heat in the Blood due to Yin Deficiency and should be used with caution during pregnancy.
contraindications: String
# In Traditional Chinese Medicine (TCM), Ai Ye belongs to the 'Herbs that invigorate the Blood' category. Like the name indicates these herbs tend to stimulate the Blood flow. In TCM they're used to help the circulation of Blood in cardiovascular conditions or menstrual irregularities as well as to treat acute pains caused by Blood Stagnation. They can also be used to treat Blood Stagnation when it causes certain tumors, cysts and hardened clots.
keyConcepts: [String]
# Flavones isolated from an extract of Artemisia Argyi were shown to have an anti-tumour effect. Seo, Jeong-Min; Kang, Hyun-Mi; Son, Kwang-Hee; Kim, Jong Han; Lee, Chang Woo; Kim, Hwan Mook; Chang, Soo-Ik; Kwon, Byoung-Mog (2003). "Antitumor Activity of Flavones Isolated fromArtemisia argyi". Planta Medica. 69 (3): 218–22. doi:10.1055/s-2003-38486.
research: [String]
# Ai Ye is also eaten as food. It is used as an ingredient in dishes such as Steamed silvery wormwood rice ball.
useAsFood: String
}
```

---