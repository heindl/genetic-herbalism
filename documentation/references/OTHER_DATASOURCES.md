# Potential Datasources

### [Henriette's Herbal Homepage](https://www.henriettes-herb.com/index.html)
> One of the most encyclopedic noncommercial web sites on herbal medicine worldwide. It contains archives on several public and non-public herbal forums, a photographic database of 8,000 botanical plant photos, and reprints of 20 major classic herbal works, which had been largely lost to history. The reprints contain information on 4,200 different plants, plus those not yet in the plant name database. (For purposes of comparison, a typical Chinese herbalist might use 300 herbs, an American herbalist 50 - 100, and a native Cherokee medicine man 800). [^1]

---

### [Dr. Duke's Phytochemical and Ethnobotanical Databases](https://phytochem.nal.usda.gov/phytochem/search)
> James A. Duke, Ph.D., the renowned ethnobotanist and retired U.S. Department of Agriculture (USDA) plant economist built this comprehensive database. This is one of the best sites for practitioners because it allows searches in a variety of ways: plant searches, activity searches, chemical searches, and ways to find ethnobotanical uses for particular plants. There is also a solid listing of useful links. A practitioner can also enter a symptom such as “wound” and search for an appropriate remedy. Best of all, there is a caveat for patients who visit this site: “The USDA does not recommend self-diagnosis or self-medication.” [^1]

#### API: Webscrape
*Unless otherwise noted, main source is: Duke, James A. 1992. Handbook of phytochemical constituents of GRAS herbs and other economic plants. Boca Raton, FL. CRC Press.*

```graphql
type Data{
name: String
commonName: String
# Ex: Food
howUsed: String
}

type Chemical{
# Ex: GLYCOLIPIDS
name: String
# Ex: Leaf
part: String
# Ex: 5504.0
lowPPM: Float
# Ex: 47448.0
highPPM: Float
# Ex: 0.26
stdDev: Float
# Ex: List, P.H. and Horhammer, L., Hager's Handbuch der Pharmazeutischen Praxis, Vols. 2-6, Springer-Verlag, Berlin, 1969-1979.
reference: String
}
```

---


### [Christopher Hobbs' Herbal Prescriber Database](https://www.christopherhobbs.com/database/?herbs&term=%)
> The Herbal Prescriber Database may be explored through Ailments and/or Herbs. Each ailment provides a treatment plan along with a list of recommended herbal remedies. Each herb includes a brief description, herbal forms, affects, cautions, dosages and a list of common ailments for which the herb is most useful.

#### API: Webscrape

```graphql
type Herb {
# Medicago sativa
latinName: String
# Lucerne
otherNames: [String!]
# Herb
partUsed: String
# Tinctures, tablets, capsules, teas, bulk herb, powder
herbForms: [String!]
# Blood
affects: String
# Has a slight blood thinning effect.
cautions: String
# A green herbaceous plant, two to three feet high with short spikes of small purple pea-like flowers.
botanicalInfo: String
# Alfalfa is a nutrient herb which stimulates the appetite, helps nourish the skin, hair, and nails, and is beneficial during convalescence. It is rich in vitamins, minerals, and chlorophyl and may have cancer protective properties. Studies have shown that alfalfa has the ability to lower serum cholesterol. Alfalfa contains phytoestrogens and makes a gentle substitute for estrogen after menopause. Alfalfa tea is used traditionally to stimulate the supply of breast milk in nursing mothers. Alfalfa has a taste of SALTY, BITTER and a temperature of WARM.
description: String
dosages: [Dosage!]
ailmentsTreated: [Ailment!]
# Newall, C. et al.. 1996. Herbal Medicines: A Guide for Health-care Professionals. London: The Pharmaceutical Press.
references: [String]
}

type Dosage {
# Infusion
type: String
# 1 cup 2 x daily
dosage: String
}

type Ailment {
# Convalescence, from long-term illness
ailment: String
# nourishing tonic
treatmentSupport: String
# tincture, tablet, capsule, tea
application: String
}
```

#### [Also contains a reverse ailments database](https://www.christopherhobbs.com/database/?ailments&term=%)

```graphql
type Ailment {
# Ex: Abdominal pain is a symptom that accompanies a number of self-resolving conditions, as diverse as stomach ulcers or gas, as well as more serious ones, such as apendicitis. If the abdominal pain becomes severe or persists, consult a qualified health care practitioner.
description: String
# Ex: Ease abdominal pain with peppermint or fennel if due to gas; gentian for overeating, and ginger due to lack of digestive power, especially when accompanied by a feeling of coldness or with nausea. Eat lightly and eat only easily digested foods, such as white rice.
treatmentPlan: String
remedies: [Remedy!]
remediesWithAdditionalAilments: [RemedyWithAdditionalAilment!]
}
type Remedy {
# Ex: Ginger
herb: String
# Ex: warming digestant
treatmentSupport: String
# Ex: tea, tincture, capsules or tablets
application: String
}
type RemedyWithAdditionalAilment {
# Ex: Abdominal pain, with diarrhea
additionalAilment: String
# Ex: Blackberry
herb: String
# Ex: tea or capsules
application: String
}
```

---



### [Memorial Sloan Kettering Cancer Center: About Herbs, Botanicals & Other Products](https://www.mskcc.org/cancer-care/diagnosis-treatment/symptom-management/integrative-medicine/herbs)
> Memorial Sloan Kettering Cancer Center’s About Herbs database, a tool for the public as well as healthcare professionals, can help you figure out the value of using common herbs and other dietary supplements.
> A pharmacist and botanicals expert manages and continually updates the database with assistance from other MSK Integrative Medicine Service experts, providing you with objective and evidence-based information that can be helpful in judging a product’s:
> - traditional and proven uses
> - potential benefits
> - possible adverse effects
> - interactions with other herbs or medicines

#### API: Web Scrape

```graphql
# https://www.mskcc.org/cancer-care/integrative-medicine/herbs/agaricus
type HerbForPatients {
# Agaricus is a mushroom. It’s used to treat many health issues. Agaricus comes as liquid extracts, teas, and capsules.
whatIsIt: String
# Agaricus is used to:
# - Treat arteriosclerosis (build-up of fats and cholesterol in and on the walls of your arteries)
# - Treat chronic hepatitis
# - Treat diabetes
# - Lower high cholesterol
whatIsItUsedFor: String
# Side effects of using agaricus may include:
# - Abnormal liver function
# - Swelling of the lip
whatAreTheSideEffects: String
# Do not take agaricus if you’re allergic to mushrooms.
whatElseDoINeedToKnow: String
}
type HerbForProfessionals {
# Agaricus blazei Murrill
scientificName: String
# Agaricus blazei is an edible mushroom native to Brazil and is cultivated in Japan for medicinal uses. It has been used to treat arteriosclerosis, hepatitis, hyperlipidemia, diabetes, dermatitis, and cancer. Preclinical studies suggest antidiabetic (8) (9), antiangiogenic, apoptotic, and antiproliferative effects (3) (4) (6) (21) (22) (24).
# Studies in humans are limited. A few suggest that agaricus extracts improved insulin resistance in diabetic patients (10) (11), reduced weight, body fat, glucose, and cholesterol levels in healthy individuals (12), and improved quality of life in patients with ulcerative colitis (30). An oral agaricus extract improved natural killer cell activity and quality of life in gynecological cancer patients undergoing chemotherapy (7). Other preliminary data show that daily intake of agaricus powder improves quality of life among cancer patients in remission (26). Although no survival improvements were reported with an agaricus extract in multiple myeloma patients, immunomodulatory effects were observed (29). However, agaricus extract did not confer any benefits in elderly women (25). Larger studies are needed to resolve the ambiguity.
# Whereas a small study reported that agaricus extract may improve liver function in patients with hepatitis B (13), liver damage and deaths (14) along with cheilitis (15) have been reported following consumption. In addition, laboratory samples of agaricus had high levels of inorganic arsenic (31). Brefeldin A, a compound isolated from agaricus, was shown to have estrogenic activity, but did not stimulate growth of breast cancer cells (27).
clinicalSummary: String
# Agaricus is an edible fungus. It is available as freeze-dried mushrooms or as concentrated liquid extracts, teas, or capsules. The whole mushroom is often added to soups, sauces, or hot teas.
foodSources: String
# Arteriosclerosis, Hepatitis, Diabetes, High Cholesterol
purportedUses: [String]
# Agaricus extract was shown to exert estrogen-like activity and may help prevent atherosclerosis via dual roles in cell signaling, macrophage development suppression and endothelial cell recovery from vascular damage (16). Both aqueous and organic extracts of agaricus offered protection to cells exposed to methyl methanesulphonate, a mutagenic agent. The stimulus produced by linoleic acid on beta-DNA polymerase, an enzyme involved in repair mechanism following exposure of DNA to alkylating agents, is thought responsible for such an effect (19).
# Ergosterol, a major constituent of agaricus, was found to inhibit tumor growth in mice via direct inhibition of tumor-induced angiogenesis (6). Other studies demonstrated that polysaccharides present in agaricus extract caused activation of macrophages (5) or natural killer cells (17) and induced cytotoxic T-lymphocyte activity in tumor-bearing mice. Specifically, activation of natural killer cells was mediated through IL-12-induced IFN-gamma expression (18). Agaricus extract stimulates caspase 3 activation and reduces telomerase activity (19) possibly through regulation of Akt signaling (20) thereby inducing apoptosis in cancer cell lines. Blazeispirol A, produced by agaricus fermentation, causes both caspase-dependent and -independent cell death in human Hep 3B cells (21). Agaritine, a hydrazine-containing constituent also exhibits antitumor activity toward U937 leukemic cells mediated through apoptosis (22). In another study, polysaccharides isolated from agaricus were shown to induce apoptosis in HL-60 cells through a signaling cascade of mitochondrial caspase-3-dependent pathway (28).
mechanismOfAction: String
# Hypersensitivity to mushrooms such as agaricus.
# In vitro, agaricus extract has estrogen-like activity (16). Therefore, patients with hormone-sensitive cancers should discuss its use with their physician.
contraindications: [String]
# Associated with hepatic dysfunction in cancer patients (14).
# Cheilitis has also been reported (15).
adverseReactions: [String]
# CYP450 substrates: In vitro, agaricus inhibits CYP3A4 and may affect the intracellular concentration of drugs metabolized by this enzyme (23). Clinical relevance has yet to be determined.
herbDrugInteractions: [String]
# May lower blood glucose levels  (8).
# May cause an elevation of liver enzymes (14).
herbLabInteractions: [String]
# Fujimiya Y, Suzuki Y, Oshiman K, et al. Selective tumoricidal effect of soluble proteoglucan extracted from the basidiomycete, Agaricus blazei Murill, mediated via natural killer cell activation and apoptosis. Cancer Immunol Immunother. May 1998;46(3):147-159.
references: [String]
}
```

---

### [Embase](https://www.embase.com/landing?status=grey)
> Improve your biomedical research with the world’s most comprehensive biomedical literature database.
- > Discover more biomedical evidence by searching comprehensive, relevant and up-to-date biomedical research
- > Create systematic searches and automate query building using intuitive search forms
- > Uncover drug-disease relationships and drug-drug interactions, manually curated from deep indexing

---

### [CAB Direct](https://www.cabdirect.org/)
> CAB Direct is the most thorough and extensive source of reference in the applied life sciences, incorporating the leading bibliographic databases CAB Abstracts and Global Health. CAB Direct provides a convenient, single point of access to all of your CABI database subscriptions.
> Review of Aromatic and Medicinal Plants (RAMP) is an abstract database of internationally published research on cultivated and wild species of culinary herbs, spices, essential oils and medicinal plants. Developed from CAB Abstracts, the original applied life sciences database, coverage includes the botany, cultivation, biological activity and use of these plants.

---

### [Transcriptome Characterization, Sequencing, And Assembly Of Medicinal Plants Relevant To Human Health](https://apps.pharmacy.uic.edu/depts/pcrps/MedTranscriptomePlants/)
The College of Pharmacy of the University of Illinois at Chicago (UIC) and its medicinal plant resource, the Dorothy Bradley Atkins Medicinal Plants Garden , is a member of a consortium to study and to understand the complete formation, storage and regulation of plant-derived medicinal compounds at the enzyme and gene level.

---

### [Prelude Medicinal Plants Database](https://www.africamuseum.be/research/collections_libraries/biology/prelude)
The PRELUDE database concerns the use of plants in different traditional veterinarian and human medicines in Africa.

---

### [Native American Ethnobotany Database](http://naeb.brit.org/)
> A Database of Foods, Drugs, Dyes and Fibers of Native American Peoples, Derived from Plants.

---

[Botanical.com: A Modern Herbal](http://www.botanical.com/botanical/mgmh/comindx.html)
> Mrs. Grieves A Modern Herbal continues to be one of the most popular resources for herbal information. Written in the early part of the last century, the advice is both timely and historical. More than 800 varieties of herbs & plants.

---

###  [Alt HealthWatch](https://www.ebsco.com/products/research-databases/alt-healthwatch)
- Alt Healthwatch is a full-text alternative health research database focused on complementary, holistic and integrated approaches to health care and wellness. It offers the latest information about the evolving practice of holistic medicine and therapies.

---

### [American Herbal Pharmacopoeia](https://herbal-ahp.org/)
> AHP produces critically reviewed herbal monographs that represent the most comprehensive and authoritative documentation on herbal medicines in the English language.

---

### [TRAMIL: Program of Applied Research to Popular Medicine in the Caribbean](https://www.tramil.net/en)
Includes Plant Database which requires registration but is free.

---

### [Hawaiian Ethnobotany Online Database](http://data.bishopmuseum.org/ethnobotanydb/ethnobotany.php?b=list&o=1)
Database of Hawaiian names, species names, and vernacular names of plants. The linked records contain information about the uses of the plants with references.

---

### [Liber Herbarum II](https://www.liberherbarum.net/)
A reference guide in multiple languages.
> The incomplete reference-guide to Herbal medicine.

---

### [Korean Traditional Knowledge Portal](https://www.koreantk.com/ktkp2014/)
> International advanced traditional knowledge prior art collection
(PCT Minimum Documentation)

---

## Footnotes
[^1]: Dighe et al., "Herbal Database Management", February 2010, https://www.bibliomed.org/mnsfulltext/196/196-1569030709.pdf?1658456263
