# Relevant Organizations

### [American Society for Pharmacognosy](https://www.pharmacognosy.us/)
Pharmacognosy is the study of drugs from natural sources. Site may have interesting resources about applied herbalism.

---

### [North American Institute of Medical Herbalism](http://www.medherb.com/)
> At the top of the portal page of this site are a series of links, boxed, with a variety of offerings. However, the more interesting contents are a bit further down. These include links such as Adverse Affects, Anatomy, Folk Uses, Plant Pharmacy, and approximately a dozen others. In many cases, these links are related to books that have been written about the topic (i.e., the use of witch hazel in the original formula for Pond’s Cold Cream). This site seems inexhaustible, being that following a trail of links can lead you to very interesting books about sixteenth-century herbal preparations. [^1]

---

###  [Foundation for Revitalisation of Local Health Traditions](http://www.frlht.org/)
> The Foundation has, together with other public interest NGOs working on traditional medical systems, argued forcefully for reorienting public health care policies, at least in India, in the direction of tapping the enormous skills and expertise available with practitioners involved in local or folk health traditions. The same groups have also insisted that more attention be paid to the Indian Systems of Medicine (ISM), notably Ayurveda, Unani, Siddha, and Tibetan Medicine. They point to the example of China, which has continued to treat its indigenous system of medicine on par with the imported Western system.

---

### [Herb Research Foundation](http://www.herbs.org)
> For over 35 years, the Herb Research Foundation has been helping to educate the world about herbs. We offer solid science and informed opinion, for free.

---

### [European Scientific Cooperative on Phytotherapy (ESCOP)](https://escop.com/)
> “Phytotherapy is the science-based medicinal use of plants and preparations derived from them, in the treatment, alleviation and / or prevention of disease or injury, according to recognised standards of quality, safety and efficacy.”

---

### [Southwest School of Botanical Medicine](https://www.swsbm.com/HOMEPAGE/HomePage.html]
Contains a number of herb manuals (some esoteric), lectures, and interesting articles such as "This is Anarcho-Herbalism"

---

### [United Plant Savers](https://unitedplantsavers.org/)
> United Plant Savers’ mission is to protect native medicinal plants, fungi, and their habitats while ensuring renewable populations for use by generations to come. Medicinal species are readily available and are harvested and cultivated with practices that protect native plants, fungi, and their habitats and that embody the principles of reciprocity, right livelihood, and biocultural diversity.

---

### [Herb Society of America](https://www.herbsociety.org/)
> The Herb Society of America is dedicated to promoting the knowledge, use and delight of herbs through educational programs, research, and sharing the experience of its members with the community.

---

### [Rocky Mountain Herbal Institute](https://www.rmhiherbal.org/tutorials/index.html)

---

### [Society for Economic Botany](http://www.econbot.org/)
> The Society for Economic Botany (SEB) is about people exploring the uses of, and our relationship with plants, cultures and our environment - plants and human affairs. You might well call our research and educational efforts, the science of survival.

---

### [Plants for a Future](https://pfaf.org/user/AboutUs.aspx)
Hosts a database of 7000 plants with edible, medicinal and other uses.
> The main aims of the charity are researching and providing information on ecologically sustainable horticulture, as an integral part of designs involving high species diversity and permaculture principles. Approaches such as woodland/forest gardening use a minimal input of resources and energy, create a harmonious eco-system and cause the least possible damage to the environment, while still having the potential to achieve high productivity.

---

### [Herb Society of America](https://www.herbsociety.org/)
> The Herb Society of America is dedicated to promoting the knowledge, use and delight of herbs through educational programs, research, and sharing the experience of its members with the community.

---

### [Medplant: Medicinal Plants Network](http://medplant.icimod.org/index.php)
An international network of people, events, and information on medicinal plants.

---

### [Chestnut School of Herbal Medicine](https://chestnutherbs.com/)
Herbalism school in Asheville that may be a good place for customer research.
Site contains a useful list of herbalism resources: https://chestnutherbs.com/about/links

---

### [American Herbalists Guild Registered Herbalists](https://www.americanherbalistsguild.com/member-profiles)
> This listing is for practitioners who have met the criteria for Registered Herbalist in the American Herbalists Guild. The information listed is provided by the practitioner and is not reviewed, evaluated or endorsed in any way by the AHG.
