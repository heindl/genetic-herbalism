# Herbal Medicine Producers & Suppliers

### [Eastern Chines Medicine Export Company](https://tcmtreatment.com/images/herb-supply/herb-price/three-lists.htm)
> Our herbs are of the best quality and warmly welcomed by clients over 220 countries in the world. We are making our price the most favored and affordable to you. We classify our hundreds herbs into three different lists according English, Latin language and Chinese Pinyin

### [IChinese Herbs](http://www.ichineseherbs.com/cross_ref_of_names.html)
> Delivering quality Botanicals, Herbs, and Concentrates to Start Up Companies, Manufactures, Students, Acupuncturists, Doctors, Veterinarians, and Herbalists. iChineseHerbs.com is a wholesale site and qualifications are required to obtain wholesale prices.  Minimum purchase is $100.00.