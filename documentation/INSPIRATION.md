# Competitors & Aspirational Applications

## Potential Competitors

### [Examine](https://examine.com/)
Pretty close to what we're working on. Membership based site with supplement guides. Should definately explore more.

### [Healthy.net](https://healthy.net/)
- Offers a wellness assessment and sells a number of new-agy products.
- Database of herbal remedies. In this past this was apparently a respected database but doesn't seem that great now.
- Owned by [HealthWorld Online](https://www.bloomberg.com/profile/company/1344086D:US)
    - From [LinkedIn](https://www.linkedin.com/company/healthworld-online/about/)
    - > E-health pioneer HealthWorld Online (www.healthy.net), was the first internet network to focus on alternative medicine, wellness, healthy living. Now in its 14th year on the web, the company provides "solutions for healthy living" to consumers and health professionals worldwide, including expert content and unique resources and services. HealthWorld licenses their innovative Wellness Inventory whole person assessment/lifestyle program (www.WellPeople.com) in the healthcare, corporate wellness, spa, fitness, and coaching industries, as well as offering it direct to the public.
    - > Wellness Inventor, a division of HealthWorld Online, is a holistic assessment and life-balance program designed to help individuals gain personal insight into their state of physical, emotional, and spiritual wellness. The program offers guidance and tools to transform this new awareness into sustainable lifestyle change, and a renewed sense of health and wellbeing. The Wellness Inventory is licensed to the corporate, healthcare, spa, fitness, and coaching industries. The Wellness Inventory Coach Certification Training has been delivered to professionals from over 12 countries.

### [Annies Remedy: Herbs for Self Healing](https://www.anniesremedy.com/)
- Contains a series of categories for narrowing down medicines for each condition.

### [Semantic Search Engine For Herbal Medicine](./inspiration_files/w210_herbert_presentation_3.pdf)
Business pitch from what looks like a student team at Berkeley. Unclear if it ever got off the ground, but mentions some interesting statistics:
- 1 billion health queries daily on Google.
- Alternative medicine accounts for 1/3 of queries.
- Herbal pharmaceutical sales account for $8 billion domestically in 2018.

### [Find Me A Cure](https://findmeacure.com/)
Simple blog with "Herbs Glossary" and "Ailments and Remedies". Does not appear to be monitized.


### [Natural Herbs Guide](https://naturalherbsguide.com/)
Simple blog from herbalist. Appears to not be monetized.

# Mass Market Apps

### [HerbList App](https://www.nccih.nih.gov/health/herblist-app)
> NCCIH's app for research-based information about the safety and effectiveness of herbal products.
> HerbList gives you fast, free access to science-based summaries on more than 50 popular herbs, such as aloe, chamomile, ginger, and turmeric. Features include:
- > Information on the herbs’ common names, history, and uses, plus what the science says about their effectiveness for health purposes
- > Easy-to-find facts on potential safety problems, side effects, and herb-drug interactions
- > A quick way to select your favorites, so that you can talk about them later with your doctor or pharmacist

### [Herbs Encyclopedia](https://play.google.com/store/apps/details?id=com.e_steps.herbs&gl=US)
Free android app.
> If you prefer natural, herbal remedies, a comprehensive resource like the Herbs Encyclopedia app is a must-have. It’s a database including a list of herbs and plants, each identified by the symptoms and health concerns it may be useful for. The app also features a list of herbs you shouldn’t take — or those that may be harmful if used incorrectly or used at all.

### [Alternative Medicine](https://play.google.com/store/apps/details?id=com.e_steps.herbs&gl=US)
Free android app.
> Consider this alternative medicine 101. This app offers information on numerous forms of alternative medicine — likely some you’ve never even heard of! Whether you’re new to the world of alternative and complementary medicine, or a professional practitioner, this app is a useful addition to your mobile library.

### [Ayurveda Home Remedies & Herbs](https://play.google.com/store/apps/details?id=com.ayurvedichomeremedies&gl=US)
Free android app.
> Ayurveda is an ancient Indian form of herbal medicine. With this app, you can learn to apply these time-tested principles to your modern life. You’ll find tips and treatments for everything from headaches to yellowing teeth. You can search by symptom or by herb.

### [Handbook of Natural Medicine](https://play.google.com/store/apps/details?id=com.mobisystems.msdict.embedded.wireless.elsevier.naturalmedicine&gl=US)
Free android app.
> If you need a comprehensive resource on natural medicine at your fingertips, this is a great resource! It’s the app version of The Clinician’s Handbook of Natural Medicine. In it, you’ll find help diagnosing and treating conditions with natural solutions. You can search by keyword or even use your camera to snap a photo of the words you wish to look up.

### [101 Natural Home Remedies Cure](https://apkpure.com/in/101-natural-home-remedies-cure/com.xlabz.homeremedies)
Free android app.
> Finding safe home remedies you can make in your kitchen can change your life, or at least the way you manage your health. This app is a great tool for locating basic home remedy recipes for a variety of symptoms. You’ll find solutions for the common cold, high blood pressure, and so much more.

### [Natural Remedies](https://play.google.com/store/apps/details?id=com.kaleidosstudio.natural_remedies&gl=US)
> This app is a comprehensive but simply designed directory of natural remedies you can find at home or in the wild. It also features vibrant photos of these remedies. You can visually identify herbs found in your backyard and determine how they may be useful in your home health care. The tool also includes news from the latest scientific studies on natural health.

### [Drugs.com Medication Guide](https://apps.apple.com/us/app/drugs-com-medication-guide/id599471042)
Free android and apple app.
> Put the entire Drugs.com database in your pocket with this easy to use app. From deciphering over-the-counter medications to visiting the pharmacy, this Drugs.com app lets you look up drug information, check for potential interactions, and even create a custom health profile with a list of your medications, allergies, and more. Quickly pull up your medication list at the doctor’s office and easily identify drugs by how they look.


## General Aesthetics

---

### [Gardenista](https://www.gardenista.com/)
A cultural news website focusing on the aesthetic of modern agrarian life.
- Provides a series of "how-to" guides for garden design.
- Sells plants, seeds, heat mats and other gardening supplies.
- Provides a directory of landscape architects, landscape designers, and other outdoor design professionals.