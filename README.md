![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

This project is currently in the **customer development** phase.

Research is kept in the [documentation](./documentation) folder. Navigate to [https://heindl.gitlab.com/genetic-herbalism](https://heindl.gitlab.io/genetic-herbalism/) for a more readable version.

---
